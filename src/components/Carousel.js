import PropTypes from 'prop-types';
import Highlight from './Highlight';
import styles from './Carousel.module.css';

const Carousel = (props) => (
    <section className={styles.container}>
        <h2 className={styles.title}>{props.title}</h2>
        <div className="carousel-container">
            <div className={styles.track}>
                {
                    !!props.items.length && props.items.map(item => <Highlight {...item} />)
                }
            </div>
        </div>
    </section>
);

Carousel.propTypes = {
    title: PropTypes.string,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            title: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired,
            images: PropTypes.arrayOf(
                PropTypes.string,
            ),
        })
    ),
};

export default Carousel;
