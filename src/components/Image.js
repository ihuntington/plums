import styles from './Image.module.css';

const Image = ({ alt, src }) => (
    <div className={styles.container}>
        <img className={styles.image} src={src} alt={alt} />
    </div>
);

export default Image;
