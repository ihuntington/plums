import styles from './LeadFeature.module.css';

const LeadFeature = ({ children }) => (
    <div className={styles.container}>
        <div className={styles.leadImage}>
            {children}
        </div>
    </div>
);

export default LeadFeature;
