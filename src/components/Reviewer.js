import PropTypes from 'prop-types';
import styles from './Reviewer.module.css';

const Reviewer = ({ name, organisation }) => (
    <div className={styles.container}>
        <span className={styles.avatar}>AA</span>
        <div className={styles.content}>
            <span>{`Home vetted by ${name}`}</span>
            {!!organisation && <span><b>{organisation}</b></span>}
        </div>
    </div>
);

Reviewer.propTypes = {
    name: PropTypes.string.isRequired,
    organisation: PropTypes.string,
};

export default Reviewer;
