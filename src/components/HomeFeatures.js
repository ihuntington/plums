import PropTypes from 'prop-types';

const HomeFeatures = (props) => (
    <div className="home-features">
        <ul className="featuresList">
            <div className="row">
                <li className="featuresListItem">{`${props.guests} guests`}</li>
                <li className="featuresListItem">{`${props.bedrooms} bedrooms`}</li>
                <li className="featuresListItem">{`${props.bathrooms} bathrooms`}</li>
            </div>
            <div className="row">
                {
                    props.stepFree && <li className="featuresListItem">Step-free entry</li>
                }
                <li className="featuresListItem">{props.size}</li>
            </div>
            <div className="row">
                {
                    !!props.transport.length && props.transport.map(
                        item => <li className="featuresListItem">{item.value}</li>
                    )
                }
            </div>
        </ul>
    </div>
);

HomeFeatures.propTypes = {
    guests: PropTypes.number.isRequired,
    bedrooms: PropTypes.number.isRequired,
    bathrooms: PropTypes.number.isRequired,
    stepFree: PropTypes.bool,
    transport: PropTypes.arrayOf(
        PropTypes.shape({
            type: PropTypes.string,
            value: PropTypes.string,
        })
    ),
};

export default HomeFeatures;
