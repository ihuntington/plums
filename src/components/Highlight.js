import Image from './Image';
import styles from './Highlight.module.css';

const Highlight = ({ images, title, text }) => (
    <div className={styles.highlight}>
        <Image src={images[0]} alt={text} />
        <span className={styles.title}>{title}</span>
        <span className={styles.text}>{text}</span>
    </div>
);

export default Highlight;
