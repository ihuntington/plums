import styles from './Buttons.module.css';

export const ReserveButton = ({ children }) => (
    <button type="submit" className={styles.primary}>{children}</button>
);

export const OutlineButton = ({ children }) => (
    <button type="submit" className={styles.outline}>{children}</button>
);
