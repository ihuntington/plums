import styles from './LeadHeader.module.css';

const LeadHeader = ({ title, subtitle }) => (
    <div className={styles.header}>
        <h2 className={styles.title}>{title}</h2>
        <span className={styles.subtitle}>{subtitle}</span>
    </div>
);

export default LeadHeader;
