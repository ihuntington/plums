import { OutlineButton, ReserveButton } from './Buttons';
import styles from './BookingBar.module.css';

const BookingBar = ({ name, locality, price, currencySymbol }) => (
    <section className={styles.container}>
        <div className={styles.place}>
            <span className={styles.title}>{name}</span>
            <span className={styles.subtitle}>{locality}</span>
        </div>
        <div className={styles.actions}>
            <div className={styles.totalStay}>
                <span className={styles.totalStayText}>Total stay</span>
                <span className={styles.currencySymbol}>{currencySymbol}</span>
                <span className={styles.price}>{price}</span>
            </div>
            <form className={styles.favourite} method="POST" action="/homes/property-name/favourite">
                <OutlineButton>Favourite</OutlineButton>
            </form>
            <form className={styles.reserve} method="POST" action="/homes/property-name/reserve">
                <input type="hidden" name="check-in" value="2020-02-08" />
                <input type="hidden" name="check-out" value="2020-02-15" />
                <ReserveButton>Reserve</ReserveButton>
            </form>
        </div>
    </section>
);

export default BookingBar;
