import PropTypes from 'prop-types';
import styles from './Review.module.css';

const Review = (props) => {
    if (!props.content.length) {
        return null;
    }

    return (
        <div>
            <h2 className={styles.title}>{props.title}</h2>
            {props.content.map(text => <p>{text}</p>)}
        </div>
    );
};

Review.propTypes = {
    title: PropTypes.string,
    content: PropTypes.arrayOf(PropTypes.string),
};

export default Review;
