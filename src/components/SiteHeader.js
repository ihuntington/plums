import Link from 'next/link';
import styles from './SiteHeader.module.css';

const SiteHeader = () => (
    <header className={styles.container}>
        <nav className={styles.nav}>
            <Link href="/homes">
                <a>Homes</a>
            </Link>
            <Link href="/hosts">
                <a>Hosts</a>
            </Link>
        </nav>
        <h1 className={styles.logo}>
            <Link href="/">
                <a>Plum Guide</a>
            </Link>
        </h1>
        <ul className={styles.tools}>
            <li>
                <Link href="/login">
                    <a>Login</a>
                </Link>
            </li>
            <li>
                <Link href="/help">
                    <a>Need help?</a>
                </Link>
            </li>
            <li>Search</li>
        </ul>
    </header>
);

export default SiteHeader;
