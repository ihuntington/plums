import styles from './Picture.module.css';

const Picture = ({ src, srcset, alt }) => (
    <picture>
        <source srcset={srcset} media="(min-width: 38rem)" />
        <img className={styles.img} src={src} alt="Example descriptive text" />
    </picture>
);

export default Picture;
