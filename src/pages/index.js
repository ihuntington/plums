import Link from 'next/link';

const Index = () => (
    <main>
        <h1>Plum Guide</h1>
        <ul>
            <li>
                <Link href="/homes/[hid]" as="/homes/kensington-church-street-vii">
                    <a>Kensington Church Street VII</a>
                </Link>
            </li>
        </ul>
    </main>
);

export default Index;
