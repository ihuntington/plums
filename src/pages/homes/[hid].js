import Head from 'next/head';
import SiteHeader from '../../components/SiteHeader';
import LeadHeader from '../../components/LeadHeader';
import LeadFeature from '../../components/LeadFeature';
import Picture from '../../components/Picture';
import BookingBar from '../../components/BookingBar';
import Review from '../../components/Review';
import Reviewer from '../../components/Reviewer';
import HomeFeatures from '../../components/HomeFeatures';
import Carousel from '../../components/Carousel';
import data from '../../mocks/homes.json';
import styles from './Home.module.css';

const Home = (props) => (
    <div className="page">
        <Head>
            <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,600&display=swap" rel="stylesheet" />
        </Head>
        <SiteHeader />
        <main>
            <div className={styles.container}>
                <LeadHeader
                    title={props.name}
                    subtitle={props.locality}
                />
                <LeadFeature>
                    <Picture src={props.images.src} srcset={props.images.srcset} />
                </LeadFeature>
                <BookingBar
                    locality={props.locality}
                    name={props.name}
                    price={props.price}
                    currencySymbol={props.currency_symbol}
                />
            </div>
            <div className={styles.contentContainer}>
                <div className={styles.content}>
                    <Review
                        title="Plum's Review"
                        content={props.review}
                    />
                </div>
                <div className={styles.aside}>
                    <Reviewer
                        name={props.reviewed_by.name}
                        organisation={props.reviewed_by.organisation}
                    />
                    <HomeFeatures
                        reviewedBy={props.reviewed_by}
                        guests={props.guests}
                        bedrooms={props.bedrooms}
                        bathrooms={props.bathrooms}
                        stepFree={props.step_free_entry}
                        size={props.size.imperial}
                        transport={props.transport}
                    />
                </div>
            </div>
            <div className={styles.container}>
                <Carousel
                    title="Handpicked highlights"
                    items={props.highlights}
                />
            </div>
        </main>
    </div>
);

Home.getInitialProps = (ctx) => {
    const { hid } = ctx.query;
    // This would be a fetch request to an API
    return data.homes[hid];
}

export default Home;
