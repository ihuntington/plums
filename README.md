# Plum Guide test

I chose to use NextJS to enable server-side rendering of the page. Once you have installed the packages you can run the project with `npm run dev` for live reloading or `npm run build` and `npm start`.

There are two entry points to demonstrate that data is requested from a JSON file based upon a URL parameter which relates to a Home ID.
- [http://localhost:3000/](http://localhost:3000/) an index page
- [http://localhost:3000/homes/kensington-church-street-vii](/homes/kensington-church-street-vii) the example Home listing page

I have roughly put the page layout together based on the PNG.

Things to note:

For the lead picture I have only used two images one for the default `src` and the other for the `srcset`. I would expand upon this by adding dpr to the media queries as well to show an appropriate image based on device resolution. Also, to use the webp format if the property images were available in that format for reduced filesize.

I've not used CSS variables in this version although I would make use of them. Depending on browser support these can be compiled to values for older IE11 for example.
